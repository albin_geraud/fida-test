import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect( page.getTitleText()).toEqual('To-do List');
  });

  it('should open Task Dialog', () => {
    page.navigateTo();
    page.getAddTaskButton().click();

    expect(page.getTaskDialogTitle()).toEqual('Add Task');
  });

  /* it('should add Task', () => {
    page.navigateTo();
    page.getTaskTitle().sendKeys('test');
    page.getSaveTaskButton().click();
    browser.pause();
    expect(page.getLastAddedTask()).toBeDefined();
  });
 */
  /* it('should cancel Task', () => {
    page.navigateTo();
    page.getCancelTaskButton().click();

    expect(page.getTaskDialogTitle()).toEqual('Add Task');
  }); */

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
