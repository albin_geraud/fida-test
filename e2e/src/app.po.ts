import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText() {
    return element(by.css('app-root .header span')).getText();
  }

  getAddTaskButton() {
    return element(by.css('.content-row button'));
  }

  getTaskDialogTitle() {
    return element(by.css('app-dialog h1')).getText();
  }

  getTaskTitle() {
    return element(by.css('app-dialog mat-form-fied:first-child'));
  }

  getSaveTaskButton() {
    return element(by.css('app-dialog .mat-dialog-actions #saveBtn'));
  }

}
