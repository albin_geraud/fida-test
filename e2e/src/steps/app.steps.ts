import { expect } from 'chai';
import { Before, Given, Then, When } from 'cucumber';
import { browser } from 'protractor';
import { AppPage } from '../app.po';

let page: AppPage;

Before(() => {
  page = new AppPage();
});

Given('I am on the home page', async () => {
  await page.navigateTo();
});

When('I do nothing', () => {});

Then('I should see the title', async () => {
  browser.get('/');
  expect(await page.getTitleText()).to.equal('To-do List');
});

When('I click on the add button', async () => {
  await page.getAddTaskButton().click();
});

Then('I should see the task dialog', async () => {
  expect(await page.getTaskDialogTitle()).to.equal('Add Task');
});
