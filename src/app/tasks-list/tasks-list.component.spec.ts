import { TaskService } from './../shared/task.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksListComponent } from './tasks-list.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';
import { Task } from '../shared/task.model';

const tasks = new BehaviorSubject<Task[]>([]);

/* const initializeTasks = () => {
  const taskList: Task[] = [new Task('manger', '', '')];
  tasks.next(taskList);
};
 */
describe('TasksListComponent', () => {
  let component: TasksListComponent;
  let fixture: ComponentFixture<TasksListComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TasksListComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call initialize tasks ', async(() => {
    const spy = spyOn(component, 'initializeTasks');
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(spy.calls.any).toBeTruthy();
    });
  }));

  it('should update tasks list', async(() => {
    const tasksService = fixture.debugElement.injector.get(TaskService);
    const taskList: Task[] = [new Task('manger', '', '')];

    tasksService.tasks.next(taskList);

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.tasks).toEqual(taskList);
    });
  }));

  it('should not be the same task object', async(() => {
    const tasksService = fixture.debugElement.injector.get(TaskService);
    const taskList: Task[] = [new Task('manger', '', '')];

    tasksService.tasks.next(taskList);

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.tasks).not.toBe([...taskList]);
    });
  }));

  it('should remove a task to the task list', async(() => {
    const task1: Task = new Task('manger', '', '');
    const task2: Task = new Task('dormir', '', '');
    const tasksList: Task[] = [task1, task2];
    localStorage.setItem('tasksList', JSON.stringify(tasksList));

    component.remove(task2);

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.tasks).toEqual([task1]);
    });
  }));
});
