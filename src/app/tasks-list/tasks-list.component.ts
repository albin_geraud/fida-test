import { TaskService } from './../shared/task.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Task } from '../shared/task.model';

export interface Fruit {
  name: string;
}

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss'],
})
export class TasksListComponent implements OnInit, OnDestroy {
  readonly visible = true;
  readonly selectable = true;
  readonly removable = true;
  public tasks: Task[];

  constructor(private taskService: TaskService) {}

  ngOnInit() {
    this.initializeTasks();
    this.taskService.tasks.subscribe((tasks: Task[]) => {
      console.log(tasks);
      this.tasks = tasks;
    });
  }

  ngOnDestroy() {
    this.taskService.tasks.unsubscribe();
  }

  remove(task: Task): void {
    this.taskService.removeTask(task);
  }

  initializeTasks() {
    this.taskService.getTasks();
  }
}
