import { TaskService } from './../shared/task.service';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Task } from '../shared/task.model';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {

  public title = '';
  public date = '';
  public description = '';

  constructor(
   private taskService: TaskService,
   public dialogRef: MatDialogRef<DialogComponent>,
   @Inject(MAT_DIALOG_DATA) public data: any
   ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSave(): void {
   this.taskService.createTask(new Task(this.title, this.description, this.date));
  }
}
