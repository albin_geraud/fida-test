import { TestBed } from '@angular/core/testing';
import { Task } from './task.model';

import { TaskService } from './task.service';

describe('TaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaskService = TestBed.inject(TaskService);
    expect(service).toBeTruthy();
  });

  it('should remove a task from the tasks list', () => {
    const service: TaskService = new TaskService();
    const task1 = new Task('mangr', '', '');
    const task2 =  new Task('dormir', '', '');
    const tasksList: Task[] = [
     task1,
     task2
    ];

    service.removeFromList(tasksList, task1);

    expect(tasksList).toEqual([task2]);
  });
});
