import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Task } from './task.model';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  public tasks = new BehaviorSubject<Task[]>([]);
  constructor() {
    // this.getTasks();
  }

  createTask(task: Task) {
    const tasksList: Task[] = JSON.parse(localStorage.getItem('tasksList'));
    if (tasksList) {
      tasksList.push(task);
      this.tasks.next(tasksList);
      localStorage.setItem('tasksList', JSON.stringify(tasksList));
    } else {
      const newTasksList = [];
      newTasksList.push(task);
      this.tasks.next(newTasksList);
      localStorage.setItem('tasksList', JSON.stringify(newTasksList));
    }
  }

  getTasks() {
    const tasksList: Task[] = JSON.parse(localStorage.getItem('tasksList'));
    if (tasksList) {
      this.tasks.next(tasksList);
    } else {
      this.tasks.next([]);
    }
  }

  removeTask(task: Task) {
    const tasksList: Task[] = JSON.parse(localStorage.getItem('tasksList'));
    this.removeFromList(tasksList, task);
    this.tasks.next(tasksList);
    localStorage.setItem('tasksList', JSON.stringify(tasksList));
  }

  removeFromList(tasksList: Task[], task: Task) {
    const index = tasksList.findIndex((obj) => obj.title === task.title);
    if (index >= 0) {
      tasksList.splice(index, 1);
    }
  }
}
